#include "Rasta.h" 
#include <iostream> 
#include<fstream>
#include <ctime> 
using namespace std;



int main() {
    ofstream fout("trial.txt");
    srand(time(NULL));
    Rasta rasta;
    rasta.initialize();

    bool** eq;
    eq = new bool* [RN];
    for (int i = 0; i < RN; i++) {
        eq[i] = new bool[ES];
    }
    int testTimes = 100;

    /* part for verification of the real value
    int count0 = 0;//linear dependent
    int count1 = 0;//number of unique solution but inequal key
    int count2 = 0;//number of unique solution and equal key
    int conflict = 0;//number of conflict
    bool sol[ES - 1];
    bool kextend[ES - 1];*/
  

    bool m[BS], k[BS], c[BS];
    int numRS1 = 0, numRS2 = 0, numRE = 0;//number for linearly dependent rows
    for (int t = 0; t < testTimes; t++) {
        for (int i = 0; i < BS; i++) {
            m[i] = 0;
            k[i] = rand() % 2;
        }
        rasta.setKey(k);

        /* key expansion
		int num = 0;
        for (int i = 0; i < BS; i++) {
            kextend[num] = k[i];
            num++;
        }
        for(int i=0;i<BS;i++)
            for (int j = i + 1; j < BS; j++) {
                kextend[num] = k[i]& k[j];
                num++;
            }
        for (int i = 0; i < BS; i++)
            for (int j = i + 1; j < BS; j++)
                for (int s = j + 1; s < BS; s++){
                    kextend[num] = k[i] & k[j] & k[s];
                    num++;
                }
        if (num != ES - 1)
            cout << "WRONG\n";*/
                


        for (int i = 0; i < RN; i++) {
            for (int j = 0; j < ES; j++) {
                eq[i][j] = 0;
            }
        }

        int dataCom = RN / (3 * BS) + 1;
        for (int i = 0; i < dataCom; i++) {
            rasta.encrypt(m, c);
            rasta.constructEqs(m, c, i, eq);//LnRGDP 0~BS-1,quadratic term,cub term,constant
        }
        //rasta.printExpr(eq, RN, ES);

        cout << endl << "current times: " << t + 1 << endl;
		
		/* record the rank of coefficient matrix*/
        int rank = rasta.rank(eq, RN, ES);
        if (rank == ES - 1)
            numRE++;
        else if (rank == ES - 2)
            numRS1++;
        else if (rank == ES - 3)
            numRS2++;
        else
            cout<<"other:"<< ES - 1 - rank << endl;


        /* verify the key expansion is the solution of the equation system
		int rank = rasta.rank(eq, ES - 1, ES);        
        cout << "Search:"<< ES - 1 - rank <<endl;
        rasta.multiplyWithMatrix(kextend, sol, eq, ES - 1);
        for (int i = 0; i < ES-1; i++)
            if (sol[i] != eq[i][ES - 1]) {
                cout << "Conflict"<<endl;
                conflict++;
                break;
            }*/
                
        /* solve the equation and verify the unique solution equal to key
		int count=rasta.gauss(eq, ES - 1, ES);
        cout << "return:" << count<<endl;
        if (count == 0)
            count0++;
        else if(count == 1)
            count1++;
        else if (count == 2)
            count2++;*/
              
        
    }
	
    /* cout << "wrong time:" << conflict << endl;
   if (count0 + count1 + count2 == testTimes) {
        cout << "count0:"<<count0<<endl;
        cout << "count1:" << count1 << endl;
        cout << "count2:" << count2 << endl;
    }*/

    cout<<"0:"<< numRE<<",1:"<<numRS1<<",2:"<<numRS2;

    rasta.freeMatrix(eq, RN);
    return 0;
}
